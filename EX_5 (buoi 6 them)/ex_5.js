function ketQua(n) {
  var flag = 1;
  // flag = 0 : không phải số nguyên tố
  // flag = 1 : là số nguyên tố

  if (n < 2) {
    return (flag = 0);
  }
  // Số nhỏ hơn 2 không phải số nguyên tố, nên trả về 0

  // Sử dụng vòng lặp while để kiểm tra có tồn tại ước số nào khác
  var i = 2;
  while (i < n) {
    if (n % i == 0) {
      flag = 0;
      break;
      // Chỉ cần tìm thấy 1 ước số là đủ và thoát vòng lặp
    }
    i++;
  }

  return flag;
}

var n = prompt("Nhập một số N: ");
// Cho user nhập vào số N

var i = 0,
  check = 0,
  result = "";
while (i <= n) {
  check = ketQua(i);
  if (check == 1) result += i + " ";
  ++i;
}

console.log(result);
