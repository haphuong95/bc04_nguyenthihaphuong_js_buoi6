function tinhTong() {
  var soX = document.getElementById("so-x").value * 1;
  var soN = document.getElementById("so-n").value * 1;
  var sum = 0;
  if (soN <= 0) {
    sum = 0;
  } else {
    for (var i = 1; i <= soN; i++) {
      // biến đếm là i
      sum += Math.pow(soX, i);
    }
  }
  document.getElementById(
    "result"
  ).innerHTML = `<h3 class="py-4"> Tổng là: ${sum.toLocaleString()}</h3>`;
}
